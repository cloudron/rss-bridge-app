FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=RSS-Bridge/rss-bridge versioning=loose
ARG RSS_BRIDGE_VERSION=2025-01-26

RUN curl -Ls https://github.com/RSS-Bridge/rss-bridge/archive/${RSS_BRIDGE_VERSION}.tar.gz | tar -xzf - --strip 1 -C /app/code \
    && chown -R www-data.www-data /app/code

RUN rm -rf /app/code/cache && \
    ln -s /app/data/cache /app/code/cache && \
    ln -s /app/data/config.ini.php /app/code/config.ini.php && \
    ln -s /app/data/whitelist.txt /app/code/whitelist.txt

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
COPY apache/rss-bridge.conf /etc/apache2/sites-enabled/rss-bridge.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN a2enmod headers

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

COPY config.ini.php start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
