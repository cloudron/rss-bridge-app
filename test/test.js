#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const BASIC_AUTH_USER = 'admin';
    const BASIC_AUTH_PASS = 'changeme123';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function checkWikipediaBridge() {
        const response = await superagent.get(`https://${app.fqdn}/?action=display&bridge=WikipediaBridge&language=en&subject=tfa&format=json`)
            .auth(BASIC_AUTH_USER, BASIC_AUTH_PASS);
        expect(response.status).to.eql(200);
        expect(response.body.home_page_url).to.contain('https://en.wikipedia.org');
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can get the main page', async function () {
        const response = await superagent.get('https://' + app.fqdn).auth(BASIC_AUTH_USER, BASIC_AUTH_PASS);
        expect(response.status).to.eql(200);
    });

    it('can use the wikipedia bridge', checkWikipediaBridge);

    it('can restart app', function () {
        execSync('cloudron restart --app ' + app.id);
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --app ${app.id} --raw`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can use the wikipedia bridge', checkWikipediaBridge);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install previous version from appstore', function () {
        execSync('cloudron install --appstore-id com.rssbridgeapp.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can use the wikipedia bridge', checkWikipediaBridge);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });

    it('can use the wikipedia bridge', checkWikipediaBridge);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
